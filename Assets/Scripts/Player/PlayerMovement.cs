﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float movementScale;
    void Update () {
        AttemptMove ();
    }

    void AttemptMove () {
        int horizontalInput = (int) Input.GetAxisRaw ("Horizontal");
        int verticalInput = (int) Input.GetAxisRaw("Vertical");

        Debug.Log(verticalInput + " | " + horizontalInput);
        if (horizontalInput != 0 || verticalInput != 0) {
            float horizontalMovement = horizontalInput * movementScale;
            float verticalMovement = verticalInput * movementScale;

            var move = new Vector3(horizontalMovement, verticalMovement, 0);
            transform.Translate (move * Time.deltaTime);
            Debug.Log(move * Time.deltaTime);
        }
    }
}
